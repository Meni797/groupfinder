import { User } from "./user";

export type Project = {
  id?: string;
  name: string;
  description: string;
  course: string;
  degreeCourse: string;
  user: User;
  timestamp: Date;
};
