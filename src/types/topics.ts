import { User } from "./user";

export type Topic = {
  id?: string;
  name: string;
  description: string;
  user: User;
  maxMembers: number;
  members: User[];
  requests: User[];
  resolved: boolean;
  timestamp: Date;
};
