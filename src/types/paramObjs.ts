export type ProjectIDParam = {
  pid: string;
};

export type TopicIDParam = {
  tid: string;
};
