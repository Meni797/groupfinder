import { Topic } from "./topics";

export type CommentProps = {
  topic: Topic;
};
