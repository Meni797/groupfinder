import { Project } from "./projects";

export type ProjectCardProps = {
  project: Project;
};
