import React from "react";
import "./App.scss";
import "firebase/firestore";
import "firebase/auth";
import Login from "./components/auth/login/Login";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from "./firebase";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Register from "./components/auth/register/Register";
import CreateProject from "./components/project/createProject/CreateProject";
import ListProjects from "./components/project/listProjects/ListProjects";
import ProjectDetail from "./components/project/projectDetail/ProjectDetail";
import Header from "./components/header/Header";
import Profile from "./components/profile/Profile";
import ProtectedRoute from "./components/protectedRoute/ProtectedRoute";
import CreateTopic from "./components/topic/createTopic/CreateTopic";
import TopicDetail from "./components/topic/topicDetail/TopicDetail";
import UpdateTopic from "./components/topic/updateTopic/UpdateTopic";
import NotFound from "./components/404/NotFound";

function App() {
  const [user] = useAuthState(auth);

  return (
    <div className='App'>
      <BrowserRouter>
        <Header></Header>
        <main>
          <Switch>
            <Route path='/' exact>
              {user ? <ListProjects /> : <Login />}
            </Route>
            <Route path='/404' exact component={NotFound}></Route>
            <Route exact path='/login' component={Login} />
            <Route exact path='/register' component={Register} />
            <ProtectedRoute
              exact
              path='/project/createProject'
              component={CreateProject}
            />
            <ProtectedRoute exact path='/projects' component={ListProjects} />
            <ProtectedRoute
              exact
              path='/project/:pid'
              component={ProjectDetail}
            />
            <ProtectedRoute
              exact
              path='/project/:pid/createTopic'
              component={CreateTopic}
            />
            <ProtectedRoute
              exact
              path='/project/:pid/topic/:tid'
              component={TopicDetail}
            />
            <ProtectedRoute
              exact
              path='/project/:pid/topic/:tid/update'
              component={UpdateTopic}
            />
            <ProtectedRoute path='/profile' component={Profile} />
            <ProtectedRoute exact path='*' component={NotFound} />
          </Switch>
        </main>
      </BrowserRouter>
    </div>
  );
}

export default App;
