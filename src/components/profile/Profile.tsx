import React from "react";
import { useHistory } from "react-router-dom";
import { auth } from "../../firebase";
import { User } from "../../types/user";
import scss from "./profile.module.scss";
import global from "../../styles/global.module.scss";
import profile from "../../assets/profile.svg";
import store from "../store/store";

const Profile = () => {
  const user: User = store.getState().user as User;

  const history = useHistory();

  const handleLogout = () => {
    auth.signOut();
    history.push("/login");
  };

  return (
    <section className={scss.profile}>
      <img src={profile} alt='profile-icon' className={scss.profileIcon} />
      <h1 className={global.titleMedium}>{user.displayName}</h1>
      <p className={scss.email}>{user.email}</p>
      <button onClick={handleLogout} className={global.buttonOrange}>
        Logout
      </button>
    </section>
  );
};

export default Profile;
