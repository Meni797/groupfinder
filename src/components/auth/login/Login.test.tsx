import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Login from "./Login";

jest.mock("../../../firebase", () => ({
  __esModule: true,
  default: () => ({
    auth: jest.fn(),
  }),
}));

describe("LoginForm", () => {
  test("Should render login component", async () => {
    render(
      <BrowserRouter>
        <Login />
      </BrowserRouter>
    );
    const loginHeading = await screen.findByText("Login");
    expect(loginHeading).toBeInTheDocument();
  });

  test("Should display button with disabled state", async () => {
    const { getByTestId } = render(
      <BrowserRouter>
        <Login />
      </BrowserRouter>
    );
    const signInButton = getByTestId("signInButton");
    expect(signInButton).toHaveAttribute("disabled");
  });

  test("Should display button without disabled state after filling out input fields", async () => {
    const { getByTestId } = render(
      <BrowserRouter>
        <Login />
      </BrowserRouter>
    );
    fireEvent.change(getByTestId("emailInput"), {
      target: { value: "john@doe.at" },
    });
    fireEvent.change(getByTestId("passwordInput"), {
      target: { value: "password" },
    });
    const signInButton = getByTestId("signInButton");
    expect(signInButton).not.toHaveAttribute("disabled");
  });
});
