import React, { useState } from "react";
import { auth } from "../../../firebase";
import { Link, useHistory } from "react-router-dom";
import scss from "../auth.module.scss";
import global from "../../../styles/global.module.scss";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const history = useHistory();
  const isFilledOutForm: boolean = email !== "" && password !== "";

  const clearErrors = () => {
    setEmailError("");
    setPasswordError("");
  };

  const handleLogin = () => {
    clearErrors();
    auth
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        history.push("/projects");
      })
      .catch((err) => {
        console.log("my error: ", err);
        switch (err.code) {
          case "auth/invalid-email":
            setEmailError(err.message);
            break;
          case "auth/user-disabled":
            setEmailError(err.message);
            break;
          case "auth/user-not-found":
            setEmailError(err.message);
            break;
          case "auth/wrong-password":
            setPasswordError(err.message);
            break;
        }
      });
  };

  return (
    <section className={global.fullHeightCentered}>
      <h1 className={global.titleBig}>Login</h1>
      {emailError && (
        <p className={global.errorMsg} data-testid="errorMsg">
          {emailError}
        </p>
      )}
      {passwordError && <p className={global.errorMsg}>{passwordError}</p>}
      <div>
        <input
          type="text"
          autoFocus
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          className={global.inputfield}
          placeholder="Username"
          aria-label="Username"
          data-testid="emailInput"
        />
        <input
          type="password"
          autoFocus
          required
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          className={global.inputfield}
          placeholder="Password"
          aria-label="Password"
          data-testid="passwordInput"
        />
        <button
          onClick={handleLogin}
          className={`${global.buttonGreen} ${scss.submitButton}`}
          disabled={!isFilledOutForm}
          data-testid="signInButton"
        >
          Sign in
        </button>
        <p>
          Don't have an account yet?{" "}
          <Link
            to="/register"
            className={scss.goToLink}
            data-testid="signUpButton"
          >
            Sign up
          </Link>
        </p>
      </div>
    </section>
  );
};

export default Login;
