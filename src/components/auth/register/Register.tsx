import React, { useState } from "react";
import { auth } from "../../../firebase";
import { Link, useHistory } from "react-router-dom";
import scss from "../auth.module.scss";
import global from "../../../styles/global.module.scss";

const Register = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const [passwordRepeat, setPasswordRepeat] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const history = useHistory();
  const isFilledOutForm: boolean =
    email !== "" && username !== "" && password !== "" && passwordRepeat !== "";

  const clearErrors = () => {
    console.log("cleared");
    setEmailError("");
    setPasswordError("");
  };

  const handleSignUp = () => {
    clearErrors();

    if (password !== passwordRepeat) {
      setPasswordError("Password is required and needs to match.");
    } else {
      auth
        .createUserWithEmailAndPassword(email, password)
        .then((userCredentials) => {
          if (userCredentials.user) {
            userCredentials.user.updateProfile({
              displayName: username,
            });
          }
        })
        .then(() => history.push("/login"))
        .catch((err) => {
          switch (err.code) {
            case "auth/invalid-email":
              setEmailError(err.message);
              break;
            case "auth/email-alread-in-use":
              setEmailError(err.message);
              break;
            case "auth/weak-password":
              setPasswordError(err.message);
              break;
            default:
              setEmailError(err.message);
          }
        });
    }
  };

  return (
    <section className={global.fullHeightCentered}>
      <h1 className={global.titleBig}>Register</h1>
      {emailError && (
        <p className={global.errorMsg} data-testid='errorMsg'>
          {emailError}
        </p>
      )}
      {passwordError && <p className={global.errorMsg}>{passwordError}</p>}
      <div>
        <input
          type='text'
          autoFocus
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          className={global.inputfield}
          placeholder='Email'
          aria-label='Email'
          data-testid='emailInput'
        />
        <input
          type='text'
          autoFocus
          required
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          className={global.inputfield}
          placeholder='Username'
          aria-label='Username'
          data-testid='usernameInput'
        />
        <input
          type='password'
          autoFocus
          required
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          className={global.inputfield}
          placeholder='Password'
          aria-label='Password'
          data-testid='passwordInput'
        />
        <input
          type='password'
          autoFocus
          required
          value={passwordRepeat}
          onChange={(e) => setPasswordRepeat(e.target.value)}
          className={global.inputfield}
          placeholder='Password repetition'
          aria-label='Password repetition'
          data-testid='passwordRepeat'
        />
        <button
          onClick={handleSignUp}
          className={`${global.buttonGreen} ${scss.submitButton}`}
          disabled={!isFilledOutForm}
          data-testid='signUpButton'
        >
          Sign up
        </button>
        <p>
          Already have an Account?{" "}
          <Link to='/login' className={scss.goToLink} data-testid='goToSignIn'>
            Sign in
          </Link>
        </p>
      </div>
    </section>
  );
};

export default Register;
