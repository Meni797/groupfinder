import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Register from "./Register";

jest.mock("../../../firebase", () => ({
  __esModule: true,
  default: () => ({
    auth: jest.fn(),
  }),
}));

describe("RegisterForm", () => {
  test("Should render register component", async () => {
    render(
      <BrowserRouter>
        <Register />
      </BrowserRouter>
    );
    const loginHeading = await screen.findByText("Register");
    expect(loginHeading).toBeInTheDocument();
  });

  test("Should display button with disabled state", async () => {
    const { getByTestId } = render(
      <BrowserRouter>
        <Register />
      </BrowserRouter>
    );
    const signUpButton = getByTestId("signUpButton");
    expect(signUpButton).toHaveAttribute("disabled");
  });

  test("Should display button without disabled state after filling out input fields", async () => {
    const { getByTestId } = render(
      <BrowserRouter>
        <Register />
      </BrowserRouter>
    );
    fireEvent.change(getByTestId("emailInput"), {
      target: { value: "john@doe.at" },
    });
    fireEvent.change(getByTestId("usernameInput"), {
      target: { value: "johnny" },
    });
    fireEvent.change(getByTestId("passwordInput"), {
      target: { value: "password" },
    });
    fireEvent.change(getByTestId("passwordRepeat"), {
      target: { value: "password" },
    });
    const signUpButton = getByTestId("signUpButton");
    expect(signUpButton).not.toHaveAttribute("disabled");
  });

  test("Should link to signIn", async () => {
    const { getByTestId } = render(
      <BrowserRouter>
        <Register />
      </BrowserRouter>
    );
    const signInButton = getByTestId("goToSignIn");
    expect(signInButton).toHaveAttribute("href");
  });
});
