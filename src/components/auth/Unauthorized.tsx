import React from "react";
import { Link } from "react-router-dom";
import { UnauthorizedProps } from "../../types/unauthorized";
import scss from "./auth.module.scss";
import global from "../../styles/global.module.scss";

const Unauthorized = ({ text }: UnauthorizedProps) => {
  return (
    <section className={global.fullHeightCentered}>
      <h1 className={global.titleBig}>Oups!</h1>
      <span>{text}</span>
      <Link to='/login' className={`${global.buttonGreen} ${scss.loginButton}`}>
        Go to Login
      </Link>
    </section>
  );
};

export default Unauthorized;
