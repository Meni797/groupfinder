import { Link } from "react-router-dom";
import global from "../../styles/global.module.scss";
import scss from "./notfound.module.scss";

const NotFound = () => {
  return (
    <section className={global.fullHeightCentered}>
      <h1 className={global.titleBig}>Failed to load resource!</h1>
      <span>
        This resource was most likely deleted or didn't exist in the first
        place!
      </span>
      <Link
        to='/projects'
        className={`${global.buttonGreen} ${scss.redirectButton}`}
      >
        Go to Projects
      </Link>
    </section>
  );
};

export default NotFound;
