import { useCallback, useState } from "react";
import { ProjectService } from "../../../firebase";
import { useParams } from "react-router-dom";
import { Comment } from "../../../types/comments";
import { User } from "../../../types/user";
import store from "../../store/store";
import send from "../../../assets/send.svg";
import scss from "./newcomment.module.scss";
import { ProjectIDParam, TopicIDParam } from "../../../types/paramObjs";

export const NewComment = () => {
  const { pid }: ProjectIDParam = useParams();
  const { tid }: TopicIDParam = useParams();
  const [message, setMessage] = useState("");

  const user: User = {
    uid: store.getState().user.uid,
    email: store.getState().user.email,
    displayName: store.getState().user.displayName
  };

  const onSend = useCallback(
    (message: string) => {
      setMessage("");

      const comment: Comment = {
        message: message,
        user: user,
        likes: [],
        timestamp: new Date()
      };

      ProjectService.getProjectById(pid)
        .collection("topics")
        .doc(tid)
        .collection("comments")
        .add(comment);
    },
    [pid, tid]
  );

  return (
    <div className={scss.newComment}>
      <input
        placeholder="Type your message ..."
        value={message}
        onChange={event => setMessage(event.target.value)}
        className={scss.messageInput}
      />
      <button onClick={() => onSend(message)} className={scss.sendButton}>
        <img src={send} alt="Send message" className={scss.sendIcon} />
      </button>
    </div>
  );
};
