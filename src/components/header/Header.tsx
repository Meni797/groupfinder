import React from "react";
import { Link, NavLink } from "react-router-dom";
import scss from "./header.module.scss";
import logo from "../../assets/groupfinder-logo.svg";

const Header = () => {
  return (
    <header className={scss.header}>
      <Link to='/' className={scss.logoLink}>
        <img className={scss.logo} src={logo} alt='logo' />
      </Link>
      <nav>
        <NavLink
          to='/projects'
          className={scss.menuItem}
          activeClassName={scss.selected}
        >
          Projects
        </NavLink>
        <NavLink
          to='/profile'
          className={scss.menuItem}
          activeClassName={scss.selected}
        >
          Profile
        </NavLink>
      </nav>
    </header>
  );
};

export default Header;
