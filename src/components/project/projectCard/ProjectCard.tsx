import { Link } from "react-router-dom";
import { ProjectCardProps } from "../../../types/projectCardProps";
import global from "../../../styles/global.module.scss";
import scss from "./projectcard.module.scss";

const ProjectCard = ({ project }: ProjectCardProps) => {
  return (
    <Link
      to={"project/" + project.id}
      className={scss.projectCard}
      data-testid='projectItem'
    >
      <div className={scss.projectCourse}>
        {project.degreeCourse} | {project.course}
      </div>
      <h2 className={global.titleMedium}>{project.name}</h2>
      <p className={scss.projectDescription}>{project.description}</p>
    </Link>
  );
};

export default ProjectCard;
