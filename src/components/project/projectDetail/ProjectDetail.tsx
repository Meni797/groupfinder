import { useEffect, useReducer, useState } from "react";
import { Topic } from "../../../types/topics";
import { ProjectService } from "../../../firebase";
import { Link, useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { Project } from "../../../types/projects";
import scss from "./projectdetail.module.scss";
import global from "../../../styles/global.module.scss";
import lightbulb from "../../../assets/lightbulb.svg";
import TopicItem from "../topicItem/TopicItem";
import { ProjectIDParam } from "../../../types/paramObjs";

const reducer = (topics: Topic[], action: Topic | undefined) => {
  if (action === undefined) {
    return [];
  } else {
    return [...topics, action];
  }
};

const ProjectDetail = () => {
  const { pid }: ProjectIDParam = useParams();
  let [project, setProject] = useState<Project | undefined>(undefined);
  const [topics, dispatch] = useReducer(reducer, []);
  const history = useHistory();

  useEffect(() => {
    const project = ProjectService.getProjectById(pid).onSnapshot(
      (snapshot) => {
        if (snapshot.data()) {
          setProject(snapshot.data() as Project);
        } else {
          history.push("/404");
        }
      }
    );

    const topicCollection = ProjectService.getProjectById(pid)
      .collection("topics")
      .orderBy("timestamp")
      .onSnapshot(
        (snapshot) => {
          dispatch(undefined);
          snapshot.docs.forEach((doc) => {
            dispatch({
              id: doc.id,
              ...doc.data()
            } as Topic);
          });
        },
        (error) => {
          console.log("Error getting documents:", error);
        }
      );

    return () => {
      project();
      topicCollection();
    };
  }, [pid, history]);

  return (
    <section className={global.fullHeightCentered}>
      <div className={scss.titleBlock}>
        <img
          src={lightbulb}
          alt="Lightbulb icon"
          className={scss.lightbulbIcon}
        />
        <div>
          <h1 className={global.titleBig}>{project?.name}</h1>
          <p>{project?.description}</p>
        </div>
      </div>

      <section className={scss.buttonRow}>
        <Link to={pid + "/createTopic"} data-testid="createTopicButton">
          <button className={global.buttonGreen}>Create Topic</button>
        </Link>
        <Link to="/projects">
          <button className={global.buttonOrange}>Project List</button>
        </Link>
      </section>
      <section className={scss.topicList}>
        {topics.map((topic, index) => (
          <TopicItem topic={topic} pid={pid} key={index}></TopicItem>
        ))}
      </section>
    </section>
  );
};

export default ProjectDetail;
