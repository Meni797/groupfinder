import { Link, useHistory } from "react-router-dom";
import { TopicItemProps } from "../../../types/topicItemProps";
import { ProjectService } from "../../../firebase";
import global from "../../../styles/global.module.scss";
import scss from "./topicitem.module.scss";
import store from "../../store/store";

const TopicItem = ({ topic, pid }: TopicItemProps) => {
  const history = useHistory();

  const deleteTopic = (topicId: string | undefined) => {
    if (topicId !== undefined) {
      ProjectService.getProjectById(pid)
        .collection("topics")
        .doc(topicId)
        .delete()
        .then((docRef) => {
          console.log("Document deleted with ID: ", docRef);
        })
        .catch((error) => {
          console.error("Error deleting document: ", error);
        });
    }
  };

  const reopenTopic = (topicId: string | undefined) => {
    if (topicId !== undefined && topic.user.uid === store.getState().user.uid) {
      ProjectService.getProjectById(pid)
        .collection("topics")
        .doc(topicId)
        .update({ resolved: false })
        .catch((error) => {
          console.error("Error resolving document: ", error);
        });
    }
  };

  return (
    <section
      className={`${topic.resolved && scss.resolvedTopic} ${scss.topicItem}`}
      data-testid='topicItem'
    >
      <Link
        to={pid + "/topic/" + topic.id}
        className={`${
          !(topic.user.uid === store.getState().user.uid) &&
          scss.topicInfoRounded
        } ${scss.topicInfo}`}
      >
        <div>
          <h2 className={global.titleSmall}>{topic.name}</h2>
          <div className={scss.topicDescription}>{topic.description}</div>
        </div>
        <div className={scss.topicInfoRight}>
          <div>{topic.user.displayName}</div>
          <div>Group size: {topic.maxMembers}</div>
        </div>
      </Link>
      {topic.user.uid === store.getState().user.uid && !topic.resolved && (
        <article className={scss.topicActions}>
          <button
            onClick={() => deleteTopic(topic.id)}
            className={`${global.buttonOrange} ${scss.topicButton}`}
          >
            Delete
          </button>
          <button
            onClick={() => history.push(pid + "/topic/" + topic.id + "/update")}
            className={`${global.buttonGreen} ${scss.topicButton}`}
          >
            Edit
          </button>
        </article>
      )}
      {topic.user.uid === store.getState().user.uid && topic.resolved && (
        <article className={scss.topicActions}>
          <button
            onClick={() => reopenTopic(topic.id)}
            className={`${global.buttonOrange} ${scss.topicButton}`}
          >
            Reopen Topic
          </button>
        </article>
      )}
    </section>
  );
};

export default TopicItem;
