import { useState } from "react";
import { firestore } from "../../../firebase";
import { Error } from "../../../types/error";
import { Project } from "../../../types/projects";
import { User } from "../../../types/user";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import store from "../../store/store";
import global from "../../../styles/global.module.scss";
import scss from "./createproject.module.scss";

const CreateProject = () => {
  const [name, setName] = useState("");
  const [errors, setErrors] = useState<Error[]>([]);
  const [description, setDescription] = useState("");
  const [course, setCourse] = useState("");
  const [degreeCourse, setDegreeCourse] = useState("");
  const isFilledOutForm: boolean =
    name !== "" && description !== "" && course !== "" && degreeCourse !== "";

  const history = useHistory();

  const user: User = {
    uid: store.getState().user.uid,
    email: store.getState().user.email,
    displayName: store.getState().user.displayName,
  };

  const clearErrors = () => {
    setErrors([]);
  };

  const handleCreateNewProject = () => {
    clearErrors();
    if (name === "") {
      setErrors((errors) => [...errors, { msg: "Name is required." }]);
    } else if (description === "") {
      setErrors((errors) => [...errors, { msg: "Description is required." }]);
    } else if (course === "") {
      setErrors((errors) => [...errors, { msg: "Course is required." }]);
    } else if (degreeCourse === "") {
      setErrors((errors) => [...errors, { msg: "DegreeCourse is required." }]);
    } else {
      const project: Project = {
        name: name,
        description: description,
        course: course,
        degreeCourse: degreeCourse,
        user: user,
        timestamp: new Date(),
      };

      firestore
        .collection("projects")
        .add(project)
        .then((docRef) => {
          history.push("/projects");
        })
        .catch((error) => {
          console.error("Error adding document: ", error);
        });
    }
  };

  return (
    <section className={global.fullHeightCentered}>
      <h1 className={global.titleBig}>Create a new Project</h1>
      <div className={global.errorMsg}>
        {errors.map((error, index) => (
          <span key={index}>{error.msg} </span>
        ))}
      </div>
      <input
        type='text'
        autoFocus
        required
        value={name}
        onChange={(e) => setName(e.target.value)}
        className={global.inputfield}
        placeholder='Project Name'
        aria-label='Project Name'
        data-testid='name'
      />
      <input
        type='text'
        autoFocus
        value={description}
        onChange={(e) => setDescription(e.target.value)}
        className={global.inputfield}
        placeholder='Description'
        aria-label='Description'
        data-testid='description'
      />
      <input
        type='text'
        autoFocus
        required
        value={course}
        onChange={(e) => setCourse(e.target.value)}
        className={global.inputfield}
        placeholder='Course'
        aria-label='Course'
        data-testid='course'
      />
      <input
        type='text'
        autoFocus
        required
        value={degreeCourse}
        onChange={(e) => setDegreeCourse(e.target.value)}
        className={global.inputfield}
        placeholder='Degree Program'
        aria-label='Degree Program'
        data-testid='degreeProgram'
      />
      <button
        onClick={handleCreateNewProject}
        className={`${global.buttonGreen} ${scss.submitButton}`}
        disabled={!isFilledOutForm}
        data-testid='createButton'
      >
        Create Project
      </button>
      <Link to='/projects'>
        <button
          className={`${global.buttonOrange} ${scss.backButton}`}
          data-testId='cancel'
        >
          Cancel
        </button>
      </Link>
    </section>
  );
};

export default CreateProject;
