import { createStore, Store } from "redux";
import userReducer from "./reducer";

const store: Store = createStore(userReducer);

export default store;
