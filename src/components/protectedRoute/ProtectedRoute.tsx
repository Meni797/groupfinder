import React from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { Route, RouteProps } from "react-router-dom";
import { auth } from "../../firebase";
import { User } from "../../types/user";
import store from "../store/store";
import loadingSpinner from "../../assets/loading-spinner.gif";
import global from "../../styles/global.module.scss";
import Unauthorized from "../auth/Unauthorized";

const ProtectedRoute = ({ ...routeProps }: RouteProps) => {
  const [user, loading, error] = useAuthState(auth);

  if (loading) {
    return (
      <div className={global.fullHeightCentered}>
        <img
          src={loadingSpinner}
          alt='loading spinner'
          width='60px'
          height='auto'
        />
      </div>
    );
  } else if (error) {
    return <Unauthorized text='An error occoured ...' />;
  } else if (user) {
    store.dispatch({ type: "SET_USER", payload: user as User });
    return <Route {...routeProps} />;
  } else {
    return <Unauthorized text='You have to be logged in to view this page!' />;
  }
};

export default ProtectedRoute;
