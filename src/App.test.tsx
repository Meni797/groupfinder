import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

describe("AppComponent", () => {
  test("Should render login headline", async () => {
    render(<App />);
    const loginHeading = await screen.findByText("Login");
    expect(loginHeading).toBeInTheDocument();
  });

  test("Should render sign up link", async () => {
    render(<App />);
    const linkElement = await screen.findByText(/Sign up/i);
    expect(linkElement).toBeInTheDocument();
  });

  test("Should render navigation", async () => {
    render(<App />);
    const projectNav = await screen.findByText("Projects");
    const profileNav = await screen.findByText("Profile");
    expect(projectNav).toBeInTheDocument();
    expect(profileNav).toBeInTheDocument();
  });
});
