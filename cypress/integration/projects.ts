describe("Project overview", () => {
  before(() => {
    cy.visit("/login");
    cy.get(`[data-testid='emailInput']`).type("mel@m.at");
    cy.get(`[data-testid='passwordInput']`).type("password");
    cy.get(`[data-testid='signInButton']`).click();
    cy.url().should("include", "/projects");
  });

  it("Should login and display project list", () => {
    cy.visit("/projects");
    cy.wait(500);
    cy.contains("Overview").should("be.visible");
  });

  it("Should create project and view it", () => {
    cy.visit("/projects");
    cy.wait(500);
    cy.get(`[data-testid='createProjectButton']`).click();
    cy.get(`[data-testid='name']`).type("Test Project");
    cy.get(`[data-testid='description']`).type(
      "A fitting description for the test project"
    );
    cy.get(`[data-testid='course']`).type("RIA");
    cy.get(`[data-testid='degreeProgram']`).type("IM");
    cy.get(`[data-testid='createButton']`).click();
    cy.url().should("include", "/projects");
    cy.contains("Test Project").click();
    cy.url().should("include", "/project/");
    cy.contains("Test Project").should("be.visible");
  });

  it("Should view existing project and go back to project list", () => {
    cy.visit("/projects");
    cy.wait(500);
    cy.get(`[data-testid='projectItem']`).first().click();
    cy.contains("Create Topic").should("be.visible");
    cy.contains("Project List").click();
    cy.url().should("include", "/projects");
  });
});
