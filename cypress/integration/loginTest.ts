describe("Login test", () => {
  it("should display login", () => {
    cy.visit("/");
    cy.contains("Login").should("be.visible");
  });

  it("button should not be disabled after filling out form", () => {
    cy.visit("/login");
    cy.get(`[data-testid='signInButton']`).should("be.disabled");
    cy.get(`[data-testid='emailInput']`).type("mel@m.at");
    cy.get(`[data-testid='passwordInput']`).type("password");
    cy.get(`[data-testid='signInButton']`).should("not.be.disabled");
  });

  it("should navigate to sign up", () => {
    cy.visit("/login");
    cy.get(`[data-testid='signUpButton']`).click();
    cy.url().should("include", "/register");
  });

  it("Should login and display project list", () => {
    cy.visit("/login");
    cy.get(`[data-testid='emailInput']`).type("mel@m.at");
    cy.get(`[data-testid='passwordInput']`).type("password");
    cy.get(`[data-testid='signInButton']`).click();
    cy.url().should("include", "/projects");
    cy.contains("Overview");
  });

  it("Should display error message for wrong credentials", () => {
    cy.visit("/login");
    cy.get(`[data-testid='errorMsg']`).should("not.exist");
    cy.get(`[data-testid='emailInput']`).type("john@j.at");
    cy.get(`[data-testid='passwordInput']`).type("password");
    cy.get(`[data-testid='signInButton']`).click();
    cy.get(`[data-testid='errorMsg']`).should("exist");
  });

  it("Should login and logout user", () => {
    cy.visit("/login");
    cy.get(`[data-testid='emailInput']`).type("mel@m.at");
    cy.get(`[data-testid='passwordInput']`).type("password");
    cy.get(`[data-testid='signInButton']`).click();
    cy.url().should("include", "/projects");
    cy.contains("Profile").click();
    cy.contains("Logout").click();
    cy.contains("Login").should("be.visible");
  });
});
