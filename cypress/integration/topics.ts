describe("Project overview", () => {
  before(() => {
    cy.visit("/login");
    cy.get(`[data-testid='emailInput']`).type("mel@m.at");
    cy.get(`[data-testid='passwordInput']`).type("password");
    cy.get(`[data-testid='signInButton']`).click();
    cy.url().should("include", "/projects");
    cy.get(`[data-testid='createProjectButton']`).click();
    cy.get(`[data-testid='name']`).type("Test Project");
    cy.get(`[data-testid='description']`).type(
      "A fitting description for the test project"
    );
    cy.get(`[data-testid='course']`).type("RIA");
    cy.get(`[data-testid='degreeProgram']`).type("IM");
    cy.get(`[data-testid='createButton']`).click();
    cy.wait(500);
  });

  it("Should create a topic and view it", () => {
    cy.get(`[data-testid='projectItem']`).first().click();
    cy.get(`[data-testid='createTopicButton']`).click();
    cy.get(`[data-testid='topicName']`).type("Test Topic");
    cy.get(`[data-testid='topicDescription']`).type(
      "A fitting description for the test topic"
    );
    cy.get(`[data-testid='createButton']`).click();
    cy.wait(500);
    cy.contains("Test Topic").click();
    cy.contains("Test Topic").should("be.visible");
    cy.go("back");
  });

  it("Should create and edit a topic", () => {
    cy.get(`[data-testid='createTopicButton']`).click();
    cy.get(`[data-testid='topicName']`).type("New Topic");
    cy.get(`[data-testid='topicDescription']`).type(
      "A fitting description for the test topic"
    );
    cy.get(`[data-testid='createButton']`).click();
    cy.wait(500);
    cy.contains("New Topic").should("exist");
    cy.contains("Edit").first().click();
    cy.wait(1000);
    cy.get(`[data-testid='topicName']`).clear().type("Updated Topic");
    cy.get(`[data-testid='updateButton']`).click();
    cy.wait(500);
    cy.contains("Updated Topic").should("exist");
  });
});
