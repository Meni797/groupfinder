describe("Register test", () => {
  it("should display register", () => {
    cy.visit("/register");
    cy.contains("Register").should("be.visible");
  });

  it("button should not be disabled after filling out form", () => {
    cy.visit("/register");
    cy.get(`[data-testid='signUpButton']`).should("be.disabled");
    cy.get(`[data-testid='emailInput']`).type("john@d.oe");
    cy.get(`[data-testid='usernameInput']`).type("john");
    cy.get(`[data-testid='passwordInput']`).type("password");
    cy.get(`[data-testid='passwordRepeat']`).type("password");
    cy.get(`[data-testid='signUpButton']`).should("not.be.disabled");
  });

  it("should navigate to sign in", () => {
    cy.visit("/register");
    cy.get(`[data-testid='goToSignIn']`).click();
    cy.url().should("include", "/login");
  });

  it("Should display error message for wrong email format", () => {
    cy.visit("/register");
    cy.get(`[data-testid='emailInput']`).type("john");
    cy.get(`[data-testid='emailInput']`).type("john");
    cy.get(`[data-testid='usernameInput']`).type("john");
    cy.get(`[data-testid='passwordInput']`).type("password");
    cy.get(`[data-testid='passwordRepeat']`).type("password");
    cy.get(`[data-testid='signUpButton']`).click();
    cy.get(`[data-testid='errorMsg']`).should("be.visible");
  });
});
