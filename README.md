# Groupfinder

Melanie Meindl - S2010629006
Lukas Menhart - S2010629007

Groupfinder aims to help people to find project groups e.g., in their studies. On the outer layer users can create Projects which users need to find project groups for. A project contains a name, a description and information about the course and the degree program. Each user can then suggest possible topics for that project. Every topic consists of a title and a description, as well as the maximum number of members alongside a comment section for ongoing discussions. If you want to participate in one of the projects you can mark your interest and will be added to a list from which the owner of the project can choose his partners. Once a group is found, the owner can close a project which would close it for modification while still being visible to others.


**Structure**

- User Page
  - Log in 
  - Log out
- Projects Page
  - Can invite other Users to this Project 
  - Topics
      - Users can suggest topics
      - Users can vote to participate/express interest in this topic
      - Creator of the topic can select/accept interested Users to a topic 
      - Topic can be set to resolved if group was found
      - Then its only viewable
      - Users can comment on a topic and leave a reaction


**Build and run instructions**

- clone the project to your local machine
- in your terminal navigate to the project folder
- run _npm install_ to install dependencies 
- you can start the application with _npm start_ -> it will run on localhost:3000
- run npm run test for executing tests with the react testing library
- run npx cypress open to open cypress and run the cypress tests (server has to run for this to work)


**Application instructions**

You have to create an account to use the application, so either use the test account or create a new user. Then you can create projects and topics as well as request and manage participation.

Test account: john@doe.at
Password: Password1
